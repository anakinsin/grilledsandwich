import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/whatis/:locale',
      name: 'lingo',
      component: () => import('./views/WhatsLinguistics.vue'),
      props: true,
    },
    {
      path: '/previous/',
      name: 'plo',
      component: () => import('./views/Plo.vue'),
    },
    {
      path: '/resource/',
      name: 'rsc',
      component: () => import('./views/Resources.vue'),
    },
    {
      path: '/resource/:year',
      name: 'rsc',
      component: () => import('./views/Resources.vue'),
    },
    {
      path: '/participate',
      name: 'part',
      component: () => import('./views/Participate.vue'),
      props: true,
    },
    {
      path: '/olympiad/:locale',
      name: 'oly',
      component: () => import('./views/WhatsOlympiad.vue'),
    },
    {
      path: '/exp',
      name: 'exp',
      component: () => import('./views/ExParticipants.vue'),
    },
  ],
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});
